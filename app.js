Vue.config.ignoredElements = [
  'a-scene',
  'a-assets',
  'a-entity',
  'a-plane',
  'a-circle',
  'a-camera',
  'a-marker-camera',
  'a-assets'
];
new Vue({
  el: '#app',
  data: {
    drawer: null,
    xSpacing: 1,
    ySpacing: 1,
    circleRadius: 1,
    fritWidth: 16,
    fritHeight: 16,
    fritOffsetX: 0,
    fritOffsetY: 0,
    fritOffsetZ: 0,
    xRotation: -90,
    yRotation: 0,
    zRotation: 0,
    dotWidth: 1,
    dotHeight: 1,
    shapeTypes: ['Rectangle', 'Circle'],
    selectedShape: 'Rectangle',
    debug: false,
    vOrient: true,
    orientations: ['90 0 0', '0 0 90']
  },
  computed: {
    grid: function () {
      let gridCoords = [];
      let xNum;
      xNum = this.fritWidth / this.xSpacing;
      yNum = this.fritHeight / this.ySpacing;
      let ix;
      for (ix = 0; ix < xNum; ix++) {
        let iy;
        for (iy = 0; iy < yNum; iy++) {
          let _x = ix * this.xSpacing;
          let _y = iy * this.ySpacing;
          gridCoords.push({
            x: _x,
            y: _y
          });
        }
      }
      return gridCoords;
    },
    orientation: function () {
      return this.vOrient ? '90 0 0' : '0 0 90';
    }
  }
});